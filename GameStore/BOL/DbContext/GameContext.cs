namespace BOL.DbContext
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class GameContext : DbContext
    {
        public GameContext()
            : base("name=GameContext")
        {
        }

        public virtual DbSet<Artifact> Artifacts { get; set; }
        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<Avatar> Avatars { get; set; }
        public virtual DbSet<CartItem> CartItems { get; set; }
        public virtual DbSet<Character> Characters { get; set; }
        public virtual DbSet<Class> Classes { get; set; }
        public virtual DbSet<Race> Races { get; set; }
        public virtual DbSet<Store> Stores { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Artifact>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Artifact>()
                .Property(e => e.Description)
                .IsFixedLength();

            modelBuilder.Entity<Artifact>()
                .Property(e => e.SellingPrice)
                .HasPrecision(5, 0);

            modelBuilder.Entity<Artifact>()
                .Property(e => e.PurchasePrice)
                .HasPrecision(5, 0);

            modelBuilder.Entity<Artifact>()
                .HasMany(e => e.CartItems)
                .WithOptional(e => e.Artifact)
                .HasForeignKey(e => e.Artifact_Id)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Artifact>()
                .HasMany(e => e.Stores)
                .WithRequired(e => e.Artifact)
                .HasForeignKey(e => e.Artif_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Artifact>()
                .HasMany(e => e.Characters)
                .WithMany(e => e.Artifacts)
                .Map(m => m.ToTable("Characters_Stock").MapLeftKey("ArtifactId_Id"));

            modelBuilder.Entity<AspNetRole>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.Characters)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.User_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Character>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Character>()
                .Property(e => e.Balance)
                .HasPrecision(10, 0);

            modelBuilder.Entity<Character>()
                .HasMany(e => e.Stores)
                .WithRequired(e => e.Character)
                .HasForeignKey(e => e.Character_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Class>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<Class>()
                .HasMany(e => e.Characters)
                .WithRequired(e => e.Class)
                .HasForeignKey(e => e.Class_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Race>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<Race>()
                .HasMany(e => e.Avatars)
                .WithRequired(e => e.Race)
                .HasForeignKey(e => e.Race_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Race>()
                .HasMany(e => e.Characters)
                .WithRequired(e => e.Race)
                .HasForeignKey(e => e.Race_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Store>()
                .Property(e => e.SellPrice)
                .HasPrecision(5, 0);

            modelBuilder.Entity<Store>()
                .Property(e => e.PurchasePrice)
                .HasPrecision(5, 0);
        }
    }
}
