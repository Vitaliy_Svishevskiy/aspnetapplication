namespace BOL.DbContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Avatar")]
    public partial class Avatar
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public int Race_Id { get; set; }

        public byte[] Image { get; set; }

        [StringLength(50)]
        public string ImageMimeType { get; set; }

        public virtual Race Race { get; set; }
    }
}
