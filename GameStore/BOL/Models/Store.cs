namespace BOL.DbContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Store")]
    public partial class Store
    {
        public int Id { get; set; }

        public int Character_Id { get; set; }

        public int Artif_Id { get; set; }

        public decimal? SellPrice { get; set; }

        public decimal? PurchasePrice { get; set; }

        public int Quantity { get; set; }

        public virtual Artifact Artifact { get; set; }

        public virtual Character Character { get; set; }
    }
}
