namespace BOL.DbContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Character")]
    public partial class Character
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Character()
        {
            Stores = new HashSet<Store>();
            Artifacts = new HashSet<Artifact>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(128)]
        public string User_Id { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        public decimal? Balance { get; set; }

        public int Race_Id { get; set; }

        public int Class_Id { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }

        public virtual Class Class { get; set; }

        public virtual Race Race { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Store> Stores { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Artifact> Artifacts { get; set; }
    }
}
