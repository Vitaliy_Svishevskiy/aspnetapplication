namespace BOL.DbContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CartItem")]
    public partial class CartItem
    {
        [Key]
        public int Cart_Id { get; set; }

        public int? Artifact_Id { get; set; }

        public int? Count { get; set; }

        [Column("CartItem")]
        [Required]
        [StringLength(50)]
        public string CartItem1 { get; set; }

        public virtual Artifact Artifact { get; set; }
    }
}
