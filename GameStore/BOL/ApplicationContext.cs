﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BOL
{
    public class ApplicationContext:IdentityDbContext<ApplicationUser>
    {
        public ApplicationContext() : base("MyDB")
        {
        }

        public static ApplicationContext CreateContext()
        {
            return new ApplicationContext();
        }
    }
}
