﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStoreUI.Areas.Secure.Models
{
    public class EditRoleModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}